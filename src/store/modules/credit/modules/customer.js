import api from '@/api'

export default {
  namespaced: true,
  state: {
    // 用户信息
    info: {}
  },
  actions: {
    customerSave ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ADD_CUSTOMER(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    customerPage ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.PAGE_CUSTOMER(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    customerUpdate ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.UPDATE_CUSTOMER(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    customerDelete ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.DELETE_CUSTOMER(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    }
  }
}
