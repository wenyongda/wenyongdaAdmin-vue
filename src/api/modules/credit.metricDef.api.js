import { creditServer } from '@/api/baseServer'
import { addMetricDefUrl, updateMetricDefUrl, deleteMetricDefUrl, listMetricDefUrl } from '@/api/baseUrl'

const baseURL = (url) => {
  return creditServer + url
}

export default ({ request }) => ({
  LIST_METRIC_DEF (data = {}) {
    return request({
      url: baseURL(listMetricDefUrl),
      method: 'post',
      data
    })
  },
  ADD_METRIC_DEF (data = {}) {
    return request({
      url: baseURL(addMetricDefUrl),
      method: 'post',
      data
    })
  },
  UPDATE_METRIC_DEF (data = {}) {
    return request({
      url: baseURL(updateMetricDefUrl),
      method: 'put',
      data
    })
  },
  DELETE_METRIC_DEF (data = {}) {
    return request({
      url: baseURL(deleteMetricDefUrl),
      method: 'delete',
      data
    })
  }
})
