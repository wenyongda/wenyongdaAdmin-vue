import { creditServer } from '@/api/baseServer'
import { addRatingTemplateUrl, updateRatingTemplateUrl, deleteRatingTemplateUrl, pageRatingTemplateUrl } from '@/api/baseUrl'

const baseURL = (url) => {
  return creditServer + url
}

export default ({ request }) => ({
  PAGE_RATING_TEMPLATE (data = {}) {
    return request({
      url: baseURL(pageRatingTemplateUrl),
      method: 'post',
      data
    })
  },
  ADD_RATING_TEMPLATE (data = {}) {
    return request({
      url: baseURL(addRatingTemplateUrl),
      method: 'post',
      data
    })
  },
  UPDATE_RATING_TEMPLATE (data = {}) {
    return request({
      url: baseURL(updateRatingTemplateUrl),
      method: 'put',
      data
    })
  },
  DELETE_RATING_TEMPLATE (data = {}) {
    return request({
      url: baseURL(deleteRatingTemplateUrl),
      method: 'delete',
      data
    })
  }
})
