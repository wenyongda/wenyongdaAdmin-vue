import api from '@/api'

export default {
  namespaced: true,
  state: {
    // 数据字典信息
    info: {}
  },
  actions: {
    /**
         * 设置数据字典
         * @param {*} param0
         * @param {*} info
         */
    async set ({ state, dispatch }, info) {
      if (!info) {
        info = await api.DICT_CURRENT()
      }
      return new Promise(resolve => {
        // store 赋值
        state.info = info
        // 持久化
        dispatch('d2admin/db/set', {
          dbName: 'sys',
          path: 'dict',
          value: info,
          user: false
        }, { root: true })
        // end
        resolve()
      })
    },
    /**
   * 从数据库取数据字典
   * @param {*} param0
   */
    getDictMap ({ state, dispatch }) {
      return new Promise(resolve => {
        // store 赋值
        state.info = dispatch('d2admin/db/get', {
          dbName: 'sys',
          path: 'dict',
          defaultValue: {},
          user: false
        }, { root: true })
        // end
        resolve(state.info)
      })
    },
    /**
         * 分页查询
         * @param {context} dispatch
         * @param {*} data
         */
    dictPage ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.DICT_PAGE_LIST(data).then(result => {
          resolve(result)
        }).catch(err => {
          reject(err)
        })
      })
    },
    /**
       * 保存
       * @param {context} dispatch
       * @param {*} data
       */
    dictSave ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.DICT_SAVE(data).then(result => {
          resolve(result)
        }).catch(err => {
          reject(err)
        })
      })
    },
    /**
       * 修改
       * @param {context} dispatch
       * @param {*} { id,data }
       */
    dictUpdate ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.DICT_UPDATE_ID(data).then(result => {
          resolve(result)
        }).catch(err => {
          reject(err)
        })
      })
    },
    /**
         * 删除
         * @param {context} dispatch
         * @param {*} data
         */
    dictDelete ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.DICT_DELETE(data).then(result => {
          resolve(result)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}
