import { seafoodSellServer } from '@/api/baseServer'
import { selectGoodPageWithCountUrl, addGoodUrl, updateGoodUrl, deleteGoodUrl, soldOutGoodUrl, onOfferGoodUrl, selectGoodIfIsDeletedPageWithCountUrl, deleteGoodCompleteUrl, recoveryGoodUrl } from '@/api/baseUrl'

const baseURL = (url) => {
  return seafoodSellServer + url
}

export default ({ request }) => ({
  GOOD_PAGE_LIST (data = {}) {
    return request({
      url: baseURL(selectGoodPageWithCountUrl),
      method: 'post',
      data
    })
  },
  GOOD_SAVE (data = {}) {
    return request({
      url: baseURL(addGoodUrl),
      method: 'post',
      data
    })
  },
  GOOD_UPDATE_ID (data = {}) {
    return request({
      url: baseURL(updateGoodUrl),
      method: 'put',
      data
    })
  },
  GOOD_DELETE (data = {}) {
    return request({
      url: baseURL(deleteGoodUrl),
      method: 'delete',
      data
    })
  },
  GOOD_SOLD_OUT (data = {}) {
    return request({
      url: baseURL(soldOutGoodUrl),
      method: 'patch',
      data
    })
  },
  GOOD_ON_OFFER (data = {}) {
    return request({
      url: baseURL(onOfferGoodUrl),
      method: 'patch',
      data
    })
  },
  GOOD_IS_DELETED_PAGE_LIST (data = {}) {
    return request({
      url: baseURL(selectGoodIfIsDeletedPageWithCountUrl),
      method: 'post',
      data
    })
  },
  GOOD_DELETE_COMPLETE (data = {}) {
    return request({
      url: baseURL(deleteGoodCompleteUrl),
      method: 'delete',
      data
    })
  },
  GOOD_RECOVERY (data = {}) {
    return request({
      url: baseURL(recoveryGoodUrl),
      method: 'patch',
      data
    })
  }
})
