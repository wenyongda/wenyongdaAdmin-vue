import api from '@/api'

export default {
  namespaced: true,
  actions: {
    roleSave ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ROLE_SAVE(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    rolePage ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ROLE_PAGE_LIST(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    roleUpdate ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ROLE_UPDATE_ID(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    roleDelete ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ROLE_DELETE(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    roleMenu ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ROLE_MENU_GET(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    roleMenuUpdate ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ROLE_MENU_UPDATE(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    roleList ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ROLE_LIST(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    }
  }
}
