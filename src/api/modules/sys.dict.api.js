
import { adminServer } from '@/api/baseServer'
import { selectDictPageWithCountUrl, addDictUrl, updateDictUrl, deleteDictUrl, getDictWithEntryUrl } from '@/api/baseUrl'

const baseURL = (url) => {
  return adminServer + url
}

export default ({ request }) => ({
  DICT_CURRENT (data = {}) {
    return request({
      url: baseURL(getDictWithEntryUrl),
      method: 'get'
    })
  },
  DICT_PAGE_LIST (data = {}) {
    return request({
      url: baseURL(selectDictPageWithCountUrl),
      method: 'post',
      data
    })
  },
  DICT_SAVE (data = {}) {
    return request({
      url: baseURL(addDictUrl),
      method: 'post',
      data
    })
  },
  DICT_UPDATE_ID (data = {}) {
    return request({
      url: baseURL(updateDictUrl),
      method: 'put',
      data
    })
  },
  DICT_DELETE (data = {}) {
    return request({
      url: baseURL(deleteDictUrl),
      method: 'delete',
      data
    })
  }

})
