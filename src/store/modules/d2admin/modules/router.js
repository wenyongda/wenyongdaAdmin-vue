
import api from '@/api'

import layoutHeaderAside from '@/layout/header-aside'
import router, { resetRouter, routesOutLayout } from '@/router'
import routes, { frameInRoutes } from '@/router/routes'
import util from '@/libs/util.js'

// 由于懒加载页面太多的话会造成webpack热更新太慢，所以开发环境不使用懒加载，只有生产环境使用懒加载
const _import = require('@/libs/util.import.' + process.env.NODE_ENV)

function createRoutesInLayout (routes = []) {
  return [{
    path: '/',
    redirect: { name: 'index' },
    component: layoutHeaderAside,
    children: [
    // 首页
      {
        path: 'index',
        name: 'index',
        meta: {
          title: '首页',
          auth: true
        },
        component: _import('system/index')
      },
      // 系统 前端日志
      {
        path: 'log',
        name: 'log',
        meta: {
          title: '前端日志',
          auth: true
        },
        component: _import('system/log')
      },
      // 刷新页面 必须保留
      {
        path: 'refresh',
        name: 'refresh',
        hidden: true,
        component: _import('system/function/refresh')
      },
      // 页面重定向 必须保留
      {
        path: 'redirect/:route*',
        name: 'redirect',
        hidden: true,
        component: _import('system/function/redirect')
      },
      // 用户设置
      {
        path: 'user-setting',
        name: 'user-setting',
        // hidden: true,
        meta: {
          title: '用户设置',
          auth: true
        },
        component: _import('system/user/info/setting')
      },
      // 个人评级模板设置
      {
        path: 'personal_rating_template_setting',
        name: 'personal_rating_template_setting',
        // hidden: true,
        meta: {
          title: '个人评级模板设置',
          auth: true
        },
        component: _import('cr/rating_template_management/personal_rating_template_setting')
      },
      ...routes
    ]
  }]
}

// const routesMock = [
//   // 演示页面
//   {
//     path: 'page1',
//     name: 'page1',
//     meta: {
//       title: '页面 1',
//       auth: true
//     },
//     component: _import('demo/page1')
//   },
//   {
//     path: 'page2',
//     name: 'page2',
//     meta: {
//       title: '页面 2',
//       auth: true
//     },
//     component: _import('demo/page2')
//   },
//   {
//     path: 'page3',
//     name: 'page3',
//     meta: {
//       title: '页面 3',
//       auth: true
//     },
//     component: _import('demo/page3')
//   },
//   {
//     path: 'usertable',
//     name: 'usertable',
//     meta: {
//       title: '用户列表',
//       auth: true
//     },
//     component: _import('user/usertable')
//   },
//   {
//     path: 'user_dialog',
//     name: 'user_dialog',
//     meta: {
//       title: '用户对话框',
//       auth: true
//     },
//     component: _import('user/user_dialog')
//   },
//   // swagger3 文档
//   {
//     path: 'swagger_ui',
//     name: 'swagger_ui',
//     meta: {
//       title: 'swagger文档',
//       auth: true
//     },
//     component: _import('swagger/swagger_ui')

//   },
//   {
//     path: 'menu',
//     name: 'menu',
//     meta: {
//       title: '菜单管理',
//       auth: true
//     },
//     component: _import('menu/menu')
//   },
//   {
//     path: 'role',
//     name: 'role',
//     meta: {
//       title: '权限管理',
//       auth: true
//     },
//     component: _import('role/role')
//   },
//   {
//     path: 'cr_person_launch',
//     name: 'cr_person_launch',
//     meta: {
//       title: '个人评级',
//       auth: true
//     },
//     component: _import('cr/cr_launch/cr_person_launch/cr_person_launch')
//   },
//   {
//     path: 'cr_business_launch',
//     name: 'cr_business_launch',
//     meta: {
//       title: '企业评级',
//       auth: true
//     },
//     component: _import('cr/cr_launch/cr_business_launch/cr_business_launch')
//   }
// ]

export default {
  namespaced: true,
  state: {
    routes: [],
    isLoad: false
  },
  mutations: {
    /**
     * @description 设置路由数据
     * @param {Object} state state
     * @param {Array} routes 路由数据
     */
    loadRoutes (state, routes) {
      state.routes = routes
    },
    /**
     * @description 设置动态路由加载状态
     * @param {Object} state state
     * @param {Boolean} isLoad 是否已经加载动态路由
     */
    isLoadedSet (state, isLoad) {
      state.isLoad = isLoad
    }
  },
  actions: {
    /**
     * @description 加载动态路由
     * @param {Object} vuex context
     * @param {Object} payload focus {Boolean} 强制重新加载动态路由 此项有值的时候加载状态校验跳过
     * @param {Object} payload to {String} 动态路由加载完成后跳转的页面
     * @param {Object} payload data {Array} 手动设置数据源 用来人工模拟权限数据或者重置权限设置 此项有值的时候登陆状态校验跳过
     */
    async load ({ state, rootState, dispatch, commit, getters }, { to = '', focus = false, data }) {
      // 加载状态校验 路由数据不存在 用户没有登录 则 取消请求 - 没有登录
      if (!data && !rootState.d2admin.account.isLogged) return
      // 加载状态校验 不强制重新加载动态路由 路由已加载 则 取消请求 - 已经加载过动态路由
      if (!focus && state.isLoad) return
      // const sourceRoutes = routesMock
      // 获取路由数据 分发action传值 或 api请求
      const sourceRoutes = buildRoutes(data || await api.ROUTER_CURRENT()) // routesMock
      commit('loadRoutes', sourceRoutes)
      const routesInLayout = createRoutesInLayout(getters.getRoutes)
      resetRouter(routesInLayout.concat(routesOutLayout))
      commit('d2admin/page/init', routesInLayout, { root: true })
      dispatch('d2admin/page/openedLoad', { filter: true }, { root: true })
      if (to) router.replace(to)
      commit('isLoadedSet', true)
    }
  },
  getters: {
    getRoutes (state) {
      return state.routes
    }
  }
}

/**
 * @description 对路由数据遍历component加上 _import()
 * @param {Array} routes 路由数据
 */
function buildRoutes (routes) {
  const routesAfterBuild = []
  routes.forEach(element => {
    element.component = _import(element.component)
    routesAfterBuild.push(element)
  })
  return routesAfterBuild
}
