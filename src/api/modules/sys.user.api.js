// import { find, assign } from 'lodash'

// const users = [
//   { username: 'admin', password: 'admin', uuid: 'admin-uuid', name: 'Admin' },
//   { username: 'editor', password: 'editor', uuid: 'editor-uuid', name: 'Editor' },
//   { username: 'user1', password: 'user1', uuid: 'user1-uuid', name: 'User1' }
// ]
// import Qs from 'qs'
import { adminServer } from '@/api/baseServer'
import { loginUrl, selectUserPageWithCountUrl, addUserUrl, updateUserUrl, deleteUserUrl, userInfoUrl, resetUserAccountPasswordUrl, updateUserAccountPasswordUrl } from '@/api/baseUrl'

const baseURL = (url) => {
  return adminServer + url
}

export default ({ service, request, serviceForMock, requestForMock, mock, faker, tools }) => ({
  /**
   * @description 登录
   * @param {Object} data 登录携带的信息
   */
  SYS_USER_LOGIN (data = {}) {
    // 模拟数据
    // mock
    //   .onAny('/login')
    //   .reply(config => {
    //     const user = find(users, tools.parse(config.data))
    //     return user
    //       ? tools.responseSuccess(assign({}, user, { token: faker.random.uuid() }))
    //       : tools.responseError({}, '账号或密码不正确')
    //   })
    // 接口请求
    return request({
      url: baseURL(loginUrl),
      method: 'post',
      data

    })
  },
  QUERY_USER_INFO (data = {}) {
    return request({
      url: baseURL(selectUserPageWithCountUrl),
      method: 'post',
      data
    })
  },
  ADD_USER (data = {}) {
    return request({
      url: baseURL(addUserUrl),
      method: 'post',
      data
    })
  },
  UPDATE_USER (data = {}) {
    return request({
      url: baseURL(updateUserUrl),
      method: 'put',
      data
    })
  },
  DELETE_USER (data = {}) {
    return request({
      url: baseURL(deleteUserUrl),
      method: 'delete',
      data
    })
  },
  USER_INFO_ID (data = {}) {
    return request({
      url: baseURL(userInfoUrl),
      method: 'get',
      params: data
    })
  },
  RESET_USER_PASSWORD (data = {}) {
    return request({
      url: baseURL(resetUserAccountPasswordUrl),
      method: 'put',
      data
    })
  },
  UPDATE_USER_PASSWORD (data = {}) {
    return request({
      url: baseURL(updateUserAccountPasswordUrl),
      method: 'put',
      data
    })
  }
})
