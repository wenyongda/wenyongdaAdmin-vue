import api from '@/api'

export default {
  namespaced: true,
  state: {
    // 用户信息
    info: {}
  },
  actions: {
    userSave ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ADD_USER(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    userPage ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.QUERY_USER_INFO(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    userUpdate ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.UPDATE_USER(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    userDelete ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.DELETE_USER(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    /**
     * 根据id重置用户密码
     * @param {*} param0
     * @param {*} param1
     */
    userRestPassword ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.RESET_USER_PASSWORD(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    /**
     * 根据id更新用户密码
     * @param {*} param0
     * @param {*} param1
     */
    updatePassword ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.UPDATE_USER_PASSWORD(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    /**
         * 根据id获取用户详情
         * @param {*} param0
         * @param {*} param1
         */
    userInfoById ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.USER_INFO_ID(data).then(result => {
          resolve(result)
        }).catch(err => {
          reject(err)
        })
      })
    },
    /**
     * @description 设置用户数据
     * @param {Object} context
     * @param {*} info info
     */
    async set ({ state, dispatch }, info) {
      // store 赋值
      state.info = info
      // 持久化
      await dispatch('d2admin/db/set', {
        dbName: 'sys',
        path: 'user.info',
        value: info,
        user: true
      }, { root: true })
    },
    /**
     * @description 从数据库取用户数据
     * @param {Object} context
     */
    async load ({ state, dispatch }) {
      // store 赋值
      state.info = await dispatch('d2admin/db/get', {
        dbName: 'sys',
        path: 'user.info',
        defaultValue: {},
        user: true
      }, { root: true })
    }
  }
}
