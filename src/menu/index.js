import { uniqueId } from 'lodash'
// import aside from './modules/sys'

/**
 * @description 给菜单数据补充上 path 字段
 * @description https://github.com/d2-projects/d2-admin/issues/209
 * @param {Array} menu 原始的菜单数据
 */
// export function supplementPath (menu) {
//   console.log(menu)
//   return menu.map(e => ({
//     ...e,
//     path: e.path || uniqueId('d2-menu-empty-'),
//     ...e.children ? {
//       children: supplementPath(e.children)
//     } : {}
//   }))
// }

export const supplementPath = (menu) => {
  return menu.map(e => ({
    ...e,
    path: e.path || uniqueId('d2-menu-empty-'),
    ...e.children ? {
      children: supplementPath(e.children)
    } : {}
  }))
}

export const menuHeader = supplementPath([
  // { path: '/index', title: '首页', icon: 'home' },
  // {
  //   title: '页面',
  //   icon: 'folder-o',
  //   children: [
  //     { path: '/page1', title: '页面 1' },
  //     { path: '/page2', title: '页面 2' },
  //     { path: '/page3', title: '页面 3' }
  //   ]
  // },
  // {
  //   title: '用户列表',
  //   icon: 'folder-o',
  //   path: '/usertable'

  // }
])

export const menuAside =
supplementPath()
// supplementPath([
//   { path: '/index', title: '首页', icon: 'home' },
//   {
//     title: '页面',
//     icon: 'folder-o',
//     children: [
//       { path: '/page1', title: '页面 1' },
//       { path: '/page2', title: '页面 2' },
//       { path: '/page3', title: '页面 3' }
//     ]
//   },
//   {
//     title: '用户中心',
//     icon: 'folder-o',
//     children: [
//       { path: '/usertable', title: '用户列表' },
//       { path: '/user_dialog', title: '用户对话框' }
//     ]

//   },
//   {
//     title: '接口文档',
//     icon: 'folder-o',
//     children: [
//       { path: '/swagger_ui', title: 'swagger文档' }
//     ]
//   },
//   {
//     title: '信评中心',
//     icon: 'folder-o',
//     children: [
//       {
//         title: '信评工作台',
//         icon: 'folder-o',
//         children: [
//           {
//             title: '信用评级待办',
//             icon: 'folder-o'

//           }
//         ]
//       },
//       {
//         title: '信用评级启动',
//         icon: 'folder-o',
//         children: [
//           {
//             title: '个人评级',
//             icon: 'folder-o',
//             path: '/cr_person_launch'
//           },
//           {
//             title: '企业评级',
//             icon: 'folder-o',
//             path: '/cr_business_launch'
//           }
//         ]
//       },
//       {
//         title: '信用评级查询',
//         icon: 'folder-o'
//       },
//       {
//         title: '评级模板管理',
//         icon: 'folder-o'
//       }
//     ]
//   }, {
//     path: '/menu', title: '菜单', icon: 'home'
//   }
// ])
