import api from '@/api'

export default {
  namespaced: true,
  state: {
  },
  actions: {
    metricDefSave ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ADD_METRIC_DEF(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    metricDefListAll ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.LIST_METRIC_DEF(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    metricDefUpdate ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.UPDATE_METRIC_DEF(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    metricDefDelete ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.DELETE_METRIC_DEF(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    }
  }
}
