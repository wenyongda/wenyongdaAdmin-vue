import layoutHeaderAside from '@/layout/header-aside'

import store from '@/store'

// 由于懒加载页面太多的话会造成webpack热更新太慢，所以开发环境不使用懒加载，只有生产环境使用懒加载
const _import = require('@/libs/util.import.' + process.env.NODE_ENV)

const files = require.context('@/router/modules', true, /\.js$/)
// 生成主框架内的路由
const generators = files.keys().map(key => files(key).default)

/**
 * 在主框架内显示
 */
// const frameIn = [JSON.parse(JSON.stringify(store.getters['d2admin/router/getRoutes']))]

const frameIn = [
  {
    path: '/',
    redirect: { name: 'index' },
    component: layoutHeaderAside,
    children: [
      // 首页
      {
        path: 'index',
        name: 'index',
        meta: {
          title: '首页',
          auth: true
        },
        component: _import('system/index')
      },
      // 系统 前端日志
      {
        path: 'log',
        name: 'log',
        meta: {
          title: '前端日志',
          auth: true
        },
        component: _import('system/log')
      },
      // 刷新页面 必须保留
      {
        path: 'refresh',
        name: 'refresh',
        hidden: true,
        component: _import('system/function/refresh')
      },
      // 页面重定向 必须保留
      {
        path: 'redirect/:route*',
        name: 'redirect',
        hidden: true,
        component: _import('system/function/redirect')
      }
      // 演示页面
      // {
      //   path: 'page1',
      //   name: 'page1',
      //   meta: {
      //     title: '页面 1',
      //     auth: true
      //   },
      //   component: _import('demo/page1')
      // },
      // {
      //   path: 'page2',
      //   name: 'page2',
      //   meta: {
      //     title: '页面 2',
      //     auth: true
      //   },
      //   component: _import('demo/page2')
      // },
      // {
      //   path: 'page3',
      //   name: 'page3',
      //   meta: {
      //     title: '页面 3',
      //     auth: true
      //   },
      //   component: _import('demo/page3')
      // },
      // {
      //   path: 'usertable',
      //   name: 'usertable',
      //   meta: {
      //     title: '用户列表',
      //     auth: true
      //   },
      //   component: _import('user/usertable')
      // },
      // {
      //   path: 'user_dialog',
      //   name: 'user_dialog',
      //   meta: {
      //     title: '用户对话框',
      //     auth: true
      //   },
      //   component: _import('user/user_dialog')
      // },
      // // swagger3 文档
      // {
      //   path: 'swagger_ui',
      //   name: 'swagger_ui',
      //   meta: {
      //     title: 'swagger文档',
      //     auth: true
      //   },
      //   component: _import('swagger/swagger_ui')

      // },
      // {
      //   path: 'menu',
      //   name: 'menu',
      //   meta: {
      //     title: '菜单管理',
      //     auth: true
      //   },
      //   component: _import('menu/menu')
      // },
      // {
      //   path: 'role',
      //   name: 'role',
      //   meta: {
      //     title: '权限管理',
      //     auth: true
      //   },
      //   component: _import('role/role')
      // },
      // {
      //   path: 'cr_person_launch',
      //   name: 'cr_person_launch',
      //   meta: {
      //     title: '个人评级',
      //     auth: true
      //   },
      //   component: _import('cr/cr_launch/cr_person_launch/cr_person_launch')
      // },
      // {
      //   path: 'cr_business_launch',
      //   name: 'cr_business_launch',
      //   meta: {
      //     title: '企业评级',
      //     auth: true
      //   },
      //   component: _import('cr/cr_launch/cr_business_launch/cr_business_launch')
      // }

    ]
  }
]

/**
 * 在主框架之外显示
 */
const frameOut = [
  // 登录
  {
    path: '/login',
    name: 'login',
    component: _import('system/login')
  }
]

/**
 * 错误页面
 */
const errorPage = [
  {
    path: '*',
    name: '404',
    component: _import('system/error/404')
  }
]

// 导出需要显示菜单的
export const frameInRoutes = frameIn

// 重新组织后导出
export default [
  ...frameIn,
  ...frameOut,
  ...errorPage
]
