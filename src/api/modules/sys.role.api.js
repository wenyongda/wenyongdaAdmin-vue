
import { adminServer } from '@/api/baseServer'
import { selectRolePageWithCountUrl, addRoleUrl, updateRoleUrl, deleteRoleUrl, getRoleMenuIdsUrl, updateRoleMenuIdsUrl, selectAllRoleUrl } from '@/api/baseUrl'

const baseURL = (url) => {
  return adminServer + url
}

export default ({ request }) => ({
  ROLE_LIST (data = {}) {
    return request({
      url: baseURL(selectAllRoleUrl),
      method: 'get',
      params: data
    })
  },
  ROLE_PAGE_LIST (data = {}) {
    return request({
      url: baseURL(selectRolePageWithCountUrl),
      method: 'post',
      data
    })
  },
  ROLE_SAVE (data = {}) {
    return request({
      url: baseURL(addRoleUrl),
      method: 'post',
      data
    })
  },
  ROLE_UPDATE_ID (data = {}) {
    return request({
      url: baseURL(updateRoleUrl),
      method: 'put',
      data
    })
  },
  ROLE_DELETE (data = {}) {
    return request({
      url: baseURL(deleteRoleUrl),
      method: 'delete',
      data
    })
  },
  ROLE_MENU_GET (data = {}) {
    return request({
      url: baseURL(getRoleMenuIdsUrl),
      method: 'get',
      params: data
    })
  },
  ROLE_MENU_UPDATE (data = {}) {
    return request({
      url: baseURL(updateRoleMenuIdsUrl),
      method: 'put',
      data
    })
  }

})
