import { adminServer } from '@/api/baseServer'
import { selectDictEntryPageWithCountUrl, addDictEntryUrl, updateDictEntryUrl, deleteDictEntryUrl } from '@/api/baseUrl'

const baseURL = (url) => {
  return adminServer + url
}

export default ({ request }) => ({
  DICT_ENTRY_PAGE_LIST (data = {}) {
    return request({
      url: baseURL(selectDictEntryPageWithCountUrl),
      method: 'post',
      data
    })
  },
  DICT_ENTRY_SAVE (data = {}) {
    return request({
      url: baseURL(addDictEntryUrl),
      method: 'post',
      data
    })
  },
  DICT_ENTRY_UPDATE_ID (data = {}) {
    return request({
      url: baseURL(updateDictEntryUrl),
      method: 'put',
      data
    })
  },
  DICT_ENTRY_DELETE (data = {}) {
    return request({
      url: baseURL(deleteDictEntryUrl),
      method: 'delete',
      data
    })
  }
})
