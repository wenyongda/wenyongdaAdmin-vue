import api from '@/api'

export default {
  namespaced: true,
  state: {
    // 个人财报模板信息
    personalRatingTemplateInfo: {}
  },
  mutations: {
    commitPersonalRatingTemplateInfo (state, payload) {
      state.personalRatingTemplateInfo = payload
    }
  },
  actions: {
    dispatchPersonalRatingTemplateInfo ({ state, dispatch }, data) {
      // context.commit('commitPersonalRatingTemplateInfo', data)
      state.personalRatingTemplateInfo = data
      dispatch('d2admin/db/set', {
        dbName: 'sys',
        path: 'personalRatingTemplateInfo',
        value: data,
        user: false
      }, { root: true })
    },
    async getPersonalRatingTemplateInfo ({ state, dispatch }) {
      state.personalRatingTemplateInfo = await dispatch('d2admin/db/get', {
        dbName: 'sys',
        path: 'personalRatingTemplateInfo',
        defaultValue: {},
        user: false
      }, { root: true })
      return state.personalRatingTemplateInfo
    },
    ratingTemplateSave ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.ADD_RATING_TEMPLATE(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    ratingTemplatePage ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.PAGE_RATING_TEMPLATE(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    ratingTemplateUpdate ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.UPDATE_RATING_TEMPLATE(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    ratingTemplateDelete ({ dispatch }, { data }) {
      return new Promise((resolve, reject) => {
        api.DELETE_RATING_TEMPLATE(data).then((result) => {
          resolve(result)
        }).catch((err) => {
          reject(err)
        })
      })
    }
  }
}
