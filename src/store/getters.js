const getters = {
  personalRatingTemplateInfo: state => state.credit.ratingTemplate.personalRatingTemplateInfo
}

export default getters
