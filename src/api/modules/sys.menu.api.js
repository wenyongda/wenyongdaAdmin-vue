import { getCurrentMenuUrl, getCurrentVueRouterUrl, addMenuTreeUrl, updateMenuTreeUrl, deleteMenuTreeUrl, getMenuTreeUrl } from '@/api/baseUrl'
import { adminServer } from '@/api/baseServer'

const baseURL = (url) => {
  return adminServer + url
}
export default ({ request }) => ({
  MENU_CURRENT (data = {}) {
    return request({
      url: baseURL(getCurrentMenuUrl),
      method: 'get',
      params: data
    })
  },
  ROUTER_CURRENT (data = {}) {
    return request({
      url: baseURL(getCurrentVueRouterUrl),
      method: 'get'
    })
  },
  MENU_QUERY_TREE (data = {}) {
    return request({
      url: baseURL(getMenuTreeUrl),
      method: 'get'
    })
  },
  MENU_TREE_SAVE (data = {}) {
    return request({
      url: baseURL(addMenuTreeUrl),
      method: 'post',
      data
    })
  },
  MENU_TREE_UPDATE (data = {}) {
    return request({
      url: baseURL(updateMenuTreeUrl),
      method: 'put',
      data
    })
  },
  MENU_TREE_DELETE (data = {}) {
    return request({
      url: baseURL(deleteMenuTreeUrl),
      method: 'delete',
      data
    })
  }
})
