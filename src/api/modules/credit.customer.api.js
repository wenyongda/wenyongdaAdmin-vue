import { creditServer } from '@/api/baseServer'
import { addCustomerUrl, updateCustomerUrl, deleteCustomerUrl, pageCustomerUrl } from '@/api/baseUrl'

const baseURL = (url) => {
  return creditServer + url
}

export default ({ request }) => ({
  PAGE_CUSTOMER (data = {}) {
    return request({
      url: baseURL(pageCustomerUrl),
      method: 'post',
      data
    })
  },
  ADD_CUSTOMER (data = {}) {
    return request({
      url: baseURL(addCustomerUrl),
      method: 'post',
      data
    })
  },
  UPDATE_CUSTOMER (data = {}) {
    return request({
      url: baseURL(updateCustomerUrl),
      method: 'put',
      data
    })
  },
  DELETE_CUSTOMER (data = {}) {
    return request({
      url: baseURL(deleteCustomerUrl),
      method: 'delete',
      data
    })
  }
})
