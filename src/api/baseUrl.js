export const loginUrl = '/login/login'

export const deleteUserUrl = '/user/delete'

export const updateUserUrl = '/user/update'

export const addUserUrl = '/user/save'

export const selectUserPageWithCountUrl = '/user/getAll'

export const userInfoUrl = '/user/info'

export const resetUserAccountPasswordUrl = '/user/reset/password'

export const updateUserAccountPasswordUrl = '/user/update/password'

export const getCurrentMenuUrl = '/menu/get/current/menutree'

export const getCurrentVueRouterUrl = '/menu/get/current/vuerouter'

export const getMenuTreeUrl = '/menu/get/menutree'

export const addMenuTreeUrl = '/menu/save/menutree'

export const updateMenuTreeUrl = '/menu/update/menutree'

export const deleteMenuTreeUrl = '/menu/delete/menutree'

export const selectDictPageWithCountUrl = '/dict/get/page'

export const addDictUrl = '/dict/save'

export const updateDictUrl = '/dict/update'

export const deleteDictUrl = '/dict/delete'

export const selectDictEntryPageWithCountUrl = '/dict/entry/get/page'

export const addDictEntryUrl = '/dict/entry/save'

export const updateDictEntryUrl = '/dict/entry/update'

export const deleteDictEntryUrl = '/dict/entry/delete'

export const getDictWithEntryUrl = '/dict/get/dict/with/entry'

export const generateCodeUrl = '/login/verifyCode/generateCode'

export const selectRolePageWithCountUrl = '/role/get/page'

export const addRoleUrl = '/role/save'

export const updateRoleUrl = '/role/update'

export const deleteRoleUrl = '/role/delete'

export const getRoleMenuIdsUrl = '/role/get/role/menu/ids'

export const updateRoleMenuIdsUrl = '/role/update/role/menu/ids'

export const selectAllRoleUrl = '/role/get/all'

export const selectGoodPageWithCountUrl = '/good/get/page'

export const addGoodUrl = '/good/save'

export const updateGoodUrl = '/good/update'

export const deleteGoodUrl = '/good/delete'

export const soldOutGoodUrl = '/good/sold/out'

export const onOfferGoodUrl = '/good/on/offer'

export const selectGoodIfIsDeletedPageWithCountUrl = '/good/get/page/is/deleted'

export const deleteGoodCompleteUrl = '/good/delete/complete'

export const recoveryGoodUrl = '/good/recovery'

export const addCustomerUrl = '/customer/save'

export const updateCustomerUrl = '/customer/update'

export const deleteCustomerUrl = '/customer/delete'

export const pageCustomerUrl = '/customer/getAll'

export const addRatingTemplateUrl = '/rating-template/save'

export const pageRatingTemplateUrl = '/rating-template/getAll'

export const updateRatingTemplateUrl = '/rating-template/update'

export const deleteRatingTemplateUrl = '/rating-template/delete'

export const addMetricDefUrl = '/metric-def/save'

export const deleteMetricDefUrl = '/metric-def/delete'

export const listMetricDefUrl = '/metric-def/getAll'

export const updateMetricDefUrl = '/metric-def/update'
